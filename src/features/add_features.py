import pandas as pd
import click

@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def add_features(input_path: str, output_path: str):
    dfxgb = pd.read_csv(input_path)
    dfxgb['Dayofyear']= dfxgb.index.day_of_year
    dfxgb['Month']= dfxgb.index.month
    FEATURES = ['Dayofyear', 'Month']
    TARGET = ['Average']
    
    dfxgb.to_csv(output_path)

if __name__ == "__main__":
    add_features()

#python -m src.features.add_features data/interim/clean_tomato.csv data/processed/featured_tomato.csv