import pandas as pd
import click

@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def clean_data(input_path: str, output_path: str):
    dfxgb = pd.read_csv(input_path)
    dfxgb = dfxbg.drop(['Maximum','Minimum','Unit','Market'], axis= 1)
    
    dfxgb.to_csv(output_path)

if __name__ == "__main__":
    clean_data()

#python -m src.data.clean_data data/raw/Tomato.csv data/interim/clean_tomato.csv