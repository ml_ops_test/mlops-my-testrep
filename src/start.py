import src 

RAW_DATA_PATH = "data/raw/Tomato.csv"
CLEANED_DATA_PATH = "data/interim/clean_tomato.csv"
FEATURED_DATAA_PATH = "data/processed/featured_tomato.csv"

if __name__ == "__main__":
    src.clean_data(RAW_DATA_PATH, CLEANED_DATA_PATH)
    src.add_features(CLEANED_DATA_PATH, FEATURED_DATAA_PATH)